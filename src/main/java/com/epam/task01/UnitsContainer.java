package com.epam.task01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UnitsContainer<T> {
    private T t;
    private List<? extends Double> listOfNumbers;
    private final static Logger LOGGER = LogManager.getLogger(UnitsContainer.class);

    public UnitsContainer() {

    }

    public T getT() {
        LOGGER.debug(t);
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public List<? extends Double> getListOfNumbers() {
        return listOfNumbers;
    }

    public void setListOfNumbers(List<? extends Double> listOfNumbers) {
        this.listOfNumbers = listOfNumbers;
    }

    public void printListOfNumbers() {
        for (Double number : listOfNumbers) {
            System.out.println(number);
        }
        LOGGER.debug(listOfNumbers);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
