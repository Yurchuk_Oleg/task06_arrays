package com.epam;

import com.epam.task01.UnitsContainer;
import com.epam.task02.ArrayChecker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public class Application {
    private final static Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Main thread started...");
        UnitsContainer<String> container = new UnitsContainer<String>();
        container.setT("Unit #1");
        System.out.println(container.getT());
        container.setT("Unit #2");
        System.out.println(container.getT());
        container.setListOfNumbers(new ArrayList<Double>(Arrays.asList(
                1.2, 1.3, 3.4, 224.5, 30.31, 50.555, 1.001
        )));
        container.printListOfNumbers();

        int[] firstArray = new int[]{10, 22, 44, 44, 777, 55, 55, 55, 22, 30};
        int[] secondArray = new int[]{10, 33, 66, 766, 777, 777, 80, 80, 888};

        new ArrayChecker();
        LOGGER.info("Elements present in both arrays");
        ArrayChecker.elemPresentInBothArrays(firstArray, secondArray);

        LOGGER.info("Elements present only in one arrays");
        ArrayChecker.elemPresentOnlyInOneArray(firstArray, secondArray);

        LOGGER.info("Remove duplicates from first array:");
        ArrayChecker.removeDuplicates(firstArray);
        LOGGER.info("Remove duplicates from second array:");
        ArrayChecker.removeDuplicates(secondArray);
    }

    public static void printArray(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
}
