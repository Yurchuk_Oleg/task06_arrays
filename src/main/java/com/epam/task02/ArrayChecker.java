package com.epam.task02;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class ArrayChecker {

    private final static Logger LOGGER = LogManager.getLogger(ArrayChecker.class);

    private static boolean contains(int[] array, int val) {
        boolean result = false;
        for (int i : array) {
            if (i == val) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static int[] elemPresentInBothArrays(int[] arr1, int[] arr2) {
        int[] resultArray = new int[Math.min(arr1.length, arr2.length)];
        int currentIndex = 0;
        for (int i : arr1) {
            if (contains(arr2, i)) {
                resultArray[currentIndex] = i;
                currentIndex++;
            }
        }
        LOGGER.debug(Arrays.copyOf(resultArray, currentIndex));
        return Arrays.copyOf(resultArray, currentIndex);
    }

    public static int[] elemPresentOnlyInOneArray(int[] arr1, int[] arr2) {
        int[] resultArray = new int[arr1.length + arr2.length];
        int firstCurrentIndex = 0;
        int secondCurrentIndex;
        for (int value : arr1) {
            if (!contains(arr2, value)) {
                resultArray[firstCurrentIndex] = value;
                firstCurrentIndex++;
            }
        }
        secondCurrentIndex = firstCurrentIndex;
        for (int value : arr2) {
            if (!contains(arr1, value)) {
                resultArray[secondCurrentIndex] = value;
                secondCurrentIndex++;
            }
        }
        LOGGER.debug(Arrays.copyOf(resultArray, secondCurrentIndex));
        return Arrays.copyOf(resultArray, secondCurrentIndex);
    }

    private static int findEndOfSequence(int[] array, int index) {
        if ((index > array.length) || (index < 0)) {
            return -1;
        }

        if (index == array.length - 1) {
            return index;
        } else if (array[index] == array[index + 1]) {
            return findEndOfSequence(array, index + 1);
        } else {
            return index;
        }
    }

    public static int[] removeDuplicates(int[] array) {
        int[] resultArray = new int[array.length];
        int resultArrayIndex = 0;

        int index = 0;
        while (index <= array.length - 1) {
            int endOfSequence = findEndOfSequence(array, index);
            resultArray[resultArrayIndex++] = array[index];
            index = endOfSequence + 1;
        }
        LOGGER.debug(Arrays.copyOf(resultArray, resultArrayIndex));
        return Arrays.copyOf(resultArray, resultArrayIndex);
    }
}